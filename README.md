

## Comunicación I2C con Arduino y sensor TMP36

En el presente documento se presentará la documentación del taller I2C de la clase de Automatización y Control de Procesos dada por Juan Manuel López King,
el cual ha sido desarrollado por los estudiantes Arnold Santiago Ríos Tovar y Daniel Esteban Torres Orjuela.

## Elementos

La simulación de la implementación se realizó en la plataforma TinkerCad, y paara la ejecución de esta se usaron

1. Dos (2) ardunios UNO R3
2. Un (1) sensor de temperatura TMP 36
3. Un (1) resistor de 100 ohms
4. Cables
5. La librería Wire de la plataforma Arduino
6. Una (1) protobard 

---
## LED
Un diodo led es un dispositivo que permite el paso de la corriente solo en un sentido, y que al ser polarizado emite un haz de luz, para su funcionamiento requiere aproximadamente de 2 voltios para funcionar y un máximo de 5 voltios según fabricante, otros valores ya requieren el uso de resistencias, tiene la ventaja de que apenas es encendido llega a su máximo rendimiento a diferencia de los que no son leds. A continuación, se muestran los valores eléctricos óptimos y máximos para el funcionamiento del led.

Para saber más de diodo, haga click [aquí](Tomado de: https://pdf1.alldatasheet.com/datasheet-pdf/view/229374/EVERLIGHT/IR323-H0-A.html)

Su utilidad es brindar iluminación al igual que un bombillo, sin embargo, en robótica y microcontroladores lo utilizan más como un testigo o una forma de indicar un suceso a través de un indicador luminoso, como en este ejemplo que es el indicador de que la temperatura que tomo el sensor del arduino esclavo supero los 30° C, por lo que el arduino maestro enciende el diodo led.

## Sensor de temperatura TMP36
Un sensor TMP36 es un dispositivo que permite medir la temperatura del ambiente en el que se encuentre, es un sensor que no mide directamente la temperatura, por el contrario usa el principio resistivo de los diodos el cual relaciona el cambio en el voltaje con un cambio en la temperatura, gracias a este principio es que logra medir la temperatura, otra cualidad importante de este sensor es que en su salida los proporciona una lectura en grados centígrados de precisión y de bajo voltaje, trabajando en un rango de voltaje de 2.7V-5.5V y con una precisión de ± 1 °C a +25 °C y ± 2 °C por encima del rango de temperatura de -40 °C a +125 °C. Comúnmente se utiliza para medir la temperatura de los sistemas de control ambiental, protección térmica, control de procesos industriales, alarmas contra incendios, entre otros, en este caso para la primera opción, medir la temperatura en el ambiente y reportar cuando supera los 30°C. A continuación, se muestran las especificaciones, los valores eléctricos óptimos y máximos para el funcionamiento del sensor TMP36.

Para saber más, consulte [el datasheet](https://www.alldatasheet.com/datasheet-pdf/pdf/49108/AD/TMP36.html)

## Implementación

Uno de los arduinos fue declarado maestro y el otro esclavo (a este se le asignó la dirección "8" para poder establecer la comunicación). Ambos se conectan mediante las entradas análogas A4(SDA) y A5(SCL),
se conectaron ambos a tierra y además, se conectó las salidas de 5V y GND del maestro a las filas de positivo y negativo de la protoboard respectivamente, al igual que los pines extremos del TMP36, mientras que el pin del centro se conectó a la entrada análoga A1 del Arduino maestro.
Finalmente se conectó un resistor al PWM 7 del Arduino esclavo, y este estaba conectado con el cátodo del LED, cuyo ánodo está conectado a negativo.

Una vez hecho esto, se realizó el código tanto para el dispositivo maestro como para el esclavo.

## Código del Arduino maestro 
```
#include <Wire.h> //La librería Wire permite la comunicación I2C entre dispositivos
int sensePin = A1;  //Pin del Arduino maestro que recibirá el voltaje dado por el sensor de temperatura
int sensorInput;    //Variable usada para almacenar el valor dado por el sensor
int DELAY = 1000; //Tiempo de espera del programa
int temp; //Variable que almacenará la temperatura en grados
double value; //variable que almacenará la proporción de temperatura 
unsigned long previousMillis = 0; // Variable usada para crear una alternativa al delay, guardará los milisegundos que lleva el programa ejecutándose cada 1000 milisegundos
 
void setup() {
  // put your setup code here, to run once:
  Wire.begin(); //Se establece la comunicación I2C entre maestro y esclavo, al no tener dirección, se aclara que este dispositivo es el maestro
  pinMode(sensePin, INPUT); //pin A1 como entrada
  Serial.begin(9600); //Se inicia el puerto serial
  
}
void loop() {
  unsigned long currentMillis = millis(); // Tiempo exacto que lleva el programa corriendo
  if (currentMillis - previousMillis >= DELAY) {
    // Con la anterior línea hacemos que el método cambiarEstado() se accione cada segundo
    previousMillis = currentMillis; // Cuando se cumplan el segundo de diferencia entre la última vez que se ejecutó el método y el tiempo de ejecución actual, previousMillis toma el valor de los milisegundos actuales
  	sensorInput = analogRead(sensePin); //Leer el valor de voltaje dado por el sensor TMP36 y almacenarlo
    
    value = (double) sensorInput / 1024;   //Se encuentra el porcentaje de la entrada leída 
  	temp = ((value * 5)-0.5)*100;  //Encontrar el valor de la temperatura según el voltaje dado 
   
    Wire.beginTransmission(8); //Se inicia una transmisión con el esclavo con dirección #8
    Serial.println(temp); //Se imprime en consola la temperatura enviada
    Wire.write(temp);  //Se envía el dato de la temperatura obtenida              
    Wire.endTransmission();  //Se cierra la transimisión
  } 
}
```

## Código del Arduino esclavo

```
#include <Wire.h> //Se incluye la librería Wire, que permite establecer una comunicación I2C entre maestro y esclavo
double temp;        // Variable que se usará para almacenar la temperatura en grados. 
int LED = 7; //Pin de salida al LED (positivo)
int DELAY = 1000; //Cantidad de milisegundos en los que el sistema se pausará
int input; // Variable usada para recibir los valores dados por el sensor de temperatura 
unsigned long previousMillis = 0; // Variable usada para crear una alternativa al delay, guardará los milisegundos que lleva el programa ejecutándose cada 1000 milisegundos

void setup() {
  Wire.begin(8); //Inicia la librería "Wire" y el dispositivo esclavo se une a la comunicación I2C teniendo la dirección #8
  Wire.onReceive(receiveEvent); //Cada que el maestro envía un dato a este esclavo, se ejecuta el método receiveEvent
  pinMode(LED, OUTPUT); // Se establece el pin LED como salida
  Serial.begin(9600); //Se inicia el puerto serial a 9600 baud
  
}
void loop() {
}

void receiveEvent(int bytes) //Cuando se recibe el evento se ejecuta este método (con el número de bytes enviados)
{
  temp = Wire.read();  //Se lee el valor enviado por el maestro (la temperatura)
    if(temp>30){ //Si la temperatura es mayor a 30, entonces se encenderá el LED
    	digitalWrite(LED, 1);
    }
    else{
      digitalWrite(LED, 0); //De lo contrario se apagará
    }  
  Serial.print("Temperatura actual: "); //Se imprime la temperatura recibida en consola
  Serial.println(temp); 

```

## Testing

Se ejecutó el código hecho en los arduinos, para ver el funcionamiento, haga click [aquí](https://unisabanaedu.sharepoint.com/:v:/s/3ID/EVxmU8so36ZPpC4g36hWOv0BvH3cdQNqW8AhgtBYizJMMQ?e=qlGiJc).

## Archivos de apoyo

Igualmente, encontrará en formato .PNG tres imágenes que representan el diagrama UML de la solución, el diagrama de actividades y la comunicación dada entre los dispositivos. 

Para saber más sobre la librería Wire, consulte [aquí](https://www.arduino.cc/en/reference/wire).